import csv
import os
from work_time_calc import DEFAULT_OUTPUT_PATH, DEFAULT_REPORT_PATH
from tests_data import TEST_DATA


TEST_RESULTS_DIR = 'tmp_test'
DEFAULT_SOURCE = 'data.xml'
SCRIP_FILE_NAME = 'work_time_calc.py'


class TestWorktimeCalc:

    # No options are given
    def test_few_employees_on_single_date(self):
        xml, expected = self.get_input_and_expected_result('test_few_employees_on_single_date')
        self.prepare_source(xml)
        os.system(f'python {SCRIP_FILE_NAME} --cli --source_path {DEFAULT_SOURCE}')
        result = self.get_csv_content(DEFAULT_OUTPUT_PATH)
        assert result == expected

    def test_few_employees_on_different_dates(self):
        xml, expected = self.get_input_and_expected_result('test_few_employees_on_different_dates')
        self.prepare_source(xml)
        os.system(f'python {SCRIP_FILE_NAME} --cli --source_path {DEFAULT_SOURCE}')
        result = self.get_csv_content(DEFAULT_OUTPUT_PATH)
        assert result == expected

    def test_employees_has_few_records_per_day(self):
        xml, expected = self.get_input_and_expected_result('test_employees_has_few_records_per_day')
        self.prepare_source(xml)
        os.system(f'python {SCRIP_FILE_NAME} --cli --source_path {DEFAULT_SOURCE}')
        result = self.get_csv_content(DEFAULT_OUTPUT_PATH)
        assert result == expected

    # All params passed
    def test_all_params_passed(self):
        xml, expected = self.get_input_and_expected_result('test_all_params_passed')
        self.prepare_source(xml)
        custom_output = os.path.join(TEST_RESULTS_DIR, 'out.csv')
        custom_report = os.path.join(TEST_RESULTS_DIR, 'report.csv')
        os.system(f'python {SCRIP_FILE_NAME} --cli --source_path {DEFAULT_SOURCE} --output_path {custom_output} --report_path {custom_report} --date_start 24-12-2011 --date_end 27-12-2011 --by_employee')
        calc_result = self.get_csv_content(custom_output)
        report_result = self.get_csv_content(custom_report)
        assert calc_result == expected[0]
        assert report_result == expected[1]

    # Parse dates options are passed
    def test_start_and_end_dates_set(self):
        xml, expected = self.get_input_and_expected_result('test_start_and_end_dates_set')
        self.prepare_source(xml)
        os.system(f'python {SCRIP_FILE_NAME} --cli --source_path {DEFAULT_SOURCE} --date_start 24-12-2011 --date_end 27-12-2011')
        result = self.get_csv_content(DEFAULT_OUTPUT_PATH)
        assert result == expected

    def test_only_start_date_is_passed(self):
        xml, expected = self.get_input_and_expected_result('test_only_start_date_is_passed')
        self.prepare_source(xml)
        os.system(f'python {SCRIP_FILE_NAME} --cli --source_path {DEFAULT_SOURCE} --date_start 24-12-2011')
        result = self.get_csv_content(DEFAULT_OUTPUT_PATH)
        assert result == expected

    def test_only_end_date_is_passed(self):
        xml, expected = self.get_input_and_expected_result('test_only_end_date_is_passed')
        self.prepare_source(xml)
        os.system(f'python {SCRIP_FILE_NAME} --cli --source_path {DEFAULT_SOURCE} --date_end 27-12-2011')
        result = self.get_csv_content(DEFAULT_OUTPUT_PATH)
        assert result == expected

    def test_start_greater_than_end(self):
        xml, expected = self.get_input_and_expected_result('test_start_greater_than_end')
        self.prepare_source(xml)
        os.system(f'python {SCRIP_FILE_NAME} --cli --source_path {DEFAULT_SOURCE}')
        result = self.get_csv_content(DEFAULT_REPORT_PATH)
        assert result == expected

    def test_end_on_another_day_than_start(self):
        xml, expected = self.get_input_and_expected_result('test_end_on_another_day_than_start')
        self.prepare_source(xml)
        os.system(f'python {SCRIP_FILE_NAME} --cli --source_path {DEFAULT_SOURCE}')
        result = self.get_csv_content(DEFAULT_REPORT_PATH)
        assert result == expected

    # Split by employee option is passed
    def test_split_by_employee(self):
        xml, expected = self.get_input_and_expected_result('test_split_by_employee')
        self.prepare_source(xml)
        os.system(f'python {SCRIP_FILE_NAME} --cli --source_path {DEFAULT_SOURCE} --by_employee')
        result = self.get_csv_content(DEFAULT_OUTPUT_PATH)
        assert result == expected

    def test_employee_has_records_on_different_days(self):
        xml, expected = self.get_input_and_expected_result('test_employee_has_records_on_different_days')
        self.prepare_source(xml)
        os.system(f'python {SCRIP_FILE_NAME} --cli --source_path {DEFAULT_SOURCE} --by_employee')
        result = self.get_csv_content(DEFAULT_OUTPUT_PATH)
        assert result == expected

    def test_employee_has_few_records_on_single_days(self):
        xml, expected = self.get_input_and_expected_result('test_employee_has_few_records_on_single_days')
        self.prepare_source(xml)
        os.system(f'python {SCRIP_FILE_NAME} --cli --source_path {DEFAULT_SOURCE} --by_employee')
        result = self.get_csv_content(DEFAULT_OUTPUT_PATH)
        assert result == expected

    # Violators recorded
    def test_no_start_date_violator_recorded(self):
        xml, expected = self.get_input_and_expected_result('test_no_start_date_violator_recorded')
        self.prepare_source(xml)
        os.system(f'python {SCRIP_FILE_NAME} --cli --source_path {DEFAULT_SOURCE}')
        result = self.get_csv_content(DEFAULT_REPORT_PATH)
        assert result == expected

    def test_no_end_date_violator_recorded(self):
        xml, expected = self.get_input_and_expected_result('test_no_end_date_violator_recorded')
        self.prepare_source(xml)
        os.system(f'python {SCRIP_FILE_NAME} --cli --source_path {DEFAULT_SOURCE}')
        result = self.get_csv_content(DEFAULT_REPORT_PATH)
        assert result == expected

    # Invalid date format is in the file
    def test_file_contains_invalid_date_format(self):
        xml, expected = self.get_input_and_expected_result('test_file_contains_invalid_date_format')
        self.prepare_source(xml)
        os.system(f'python {SCRIP_FILE_NAME} --cli --source_path {DEFAULT_SOURCE} > {DEFAULT_OUTPUT_PATH}')
        result = self.get_csv_content(DEFAULT_OUTPUT_PATH)
        assert result == expected

    # File path option passed
    def test_output_file_path_option_respected(self):
        xml, expected = self.get_input_and_expected_result('test_output_file_path_option_respected')
        self.prepare_source(xml)
        custom_output = os.path.join(TEST_RESULTS_DIR, 'test_file.csv')
        os.system(f'python {SCRIP_FILE_NAME} --cli --source_path {DEFAULT_SOURCE} --output_path {custom_output}')
        result = self.get_csv_content(custom_output)
        assert result == expected

    def test_report_file_path_option_respected(self):
        xml, expected = self.get_input_and_expected_result('test_output_file_path_option_respected')
        self.prepare_source(xml)
        custom_report = os.path.join(TEST_RESULTS_DIR, 'test_file2.csv')
        os.system(f'python {SCRIP_FILE_NAME} --cli --source_path {DEFAULT_SOURCE} --output_path {custom_report}')
        result = self.get_csv_content(custom_report)
        assert result == expected

    # Utilities
    def get_csv_content(self, file_path):
        result = []
        with open(file_path, encoding='utf-8') as csv_file:
            reader = csv.reader(csv_file, delimiter=',')
            for row in reader:
                result.append(row)
        return result

    def get_input_and_expected_result(self, test_name):
        test_data = TEST_DATA[test_name]
        return test_data['input'], test_data['expected']

    def prepare_source(self, xml):
        input_file = DEFAULT_SOURCE
        with open(input_file, 'w', encoding='utf-8') as source_file:
            source_file.write(xml)
