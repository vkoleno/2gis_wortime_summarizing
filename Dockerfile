FROM amd64/python:3.7.6-alpine
RUN apk add --update --no-cache g++ gcc libxslt-dev
RUN mkdir app
RUN chmod -R 755 /app
WORKDIR /app
COPY . /app
RUN pip install -r /app/requirements.txt
EXPOSE 5000
ENV FLASK_APP=work_time_calc
CMD ["flask", "run", "--host=0.0.0.0"]