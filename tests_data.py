# In this file mindfully ignored max string length limitation
TEST_DATA = {
    'test_few_employees_on_single_date': {
        'input': """<?xml version="1.0" encoding="UTF-8"?>
        <people>
            <person full_name="i.ivanov">
                <start>21-12-2011 10:00:00</start>
                <end>21-12-2011 19:00:00</end>
            </person>
            <person full_name="s.petrov">
                <start>21-12-2011 11:00:00</start>
                <end>21-12-2011 18:00:00</end>
            </person>
        </people>""",
        'expected': [['21-12-2011', 'all', '16:00:00']],
    },
    'test_few_employees_on_different_dates': {
        'input': """<?xml version="1.0" encoding="UTF-8"?>
        <people>
            <person full_name="i.ivanov">
                <start>21-12-2011 10:00:00</start>
                <end>21-12-2011 19:00:00</end>
            </person>
            <person full_name="s.petrov">
                <start>21-12-2011 11:00:00</start>
                <end>21-12-2011 18:00:00</end>
            </person>
            <person full_name="i.ivanov">
                <start>22-12-2011 10:00:00</start>
                <end>22-12-2011 19:00:00</end>
            </person>
            <person full_name="s.petrov">
                <start>22-12-2011 11:00:00</start>
                <end>22-12-2011 17:00:00</end>
            </person>
        </people>""",
        'expected': [['21-12-2011', 'all', '16:00:00'], ['22-12-2011', 'all', '15:00:00']],
    },
    'test_employees_has_few_records_per_day': {
        'input': """<?xml version="1.0" encoding="UTF-8"?>
        <people>
            <person full_name="i.ivanov">
                <start>21-12-2011 10:00:00</start>
                <end>21-12-2011 12:00:00</end>
            </person>
            <person full_name="s.petrov">
                <start>21-12-2011 11:00:00</start>
                <end>21-12-2011 18:00:00</end>
            </person>
            <person full_name="i.ivanov">
                <start>21-12-2011 13:00:00</start>
                <end>21-12-2011 19:00:00</end>
            </person>
        </people>""",
        'expected': [['21-12-2011', 'all', '15:00:00']],
    },
    'test_start_and_end_dates_set': {
        'input': """<?xml version="1.0" encoding="UTF-8"?>
        <people>
            <person full_name="i.ivanov">
                <start>21-12-2011 10:00:00</start>
                <end>21-12-2011 12:00:00</end>
            </person>
            <person full_name="s.petrov">
                <start>25-12-2011 11:00:00</start>
                <end>25-12-2011 18:00:00</end>
            </person>
            <person full_name="i.ivanov">
                <start>28-12-2011 13:00:00</start>
                <end>28-12-2011 19:00:00</end>
            </person>
        </people>""",
        'expected': [['25-12-2011', 'all', '7:00:00']],
    },
    'test_only_start_date_is_passed': {
        'input': """<?xml version="1.0" encoding="UTF-8"?>
        <people>
            <person full_name="i.ivanov">
                <start>21-12-2011 10:00:00</start>
                <end>21-12-2011 12:00:00</end>
            </person>
            <person full_name="s.petrov">
                <start>25-12-2011 11:00:00</start>
                <end>25-12-2011 18:00:00</end>
            </person>
            <person full_name="i.ivanov">
                <start>28-12-2011 13:00:00</start>
                <end>28-12-2011 19:00:00</end>
            </person>
        </people>""",
        'expected': [['25-12-2011', 'all', '7:00:00'], ['28-12-2011', 'all', '6:00:00']],
    },
    'test_only_end_date_is_passed': {
        'input': """<?xml version="1.0" encoding="UTF-8"?>
        <people>
            <person full_name="i.ivanov">
                <start>21-12-2011 10:00:00</start>
                <end>21-12-2011 12:00:00</end>
            </person>
            <person full_name="s.petrov">
                <start>25-12-2011 11:00:00</start>
                <end>25-12-2011 18:00:00</end>
            </person>
            <person full_name="i.ivanov">
                <start>28-12-2011 13:00:00</start>
                <end>28-12-2011 19:00:00</end>
            </person>
        </people>""",
        'expected': [['21-12-2011', 'all', '2:00:00'], ['25-12-2011', 'all', '7:00:00']],
    },
    'test_start_greater_than_end': {
        'input': """<?xml version="1.0" encoding="UTF-8"?>
        <people>
            <person full_name="i.ivanov">
                <start>22-12-2011 10:00:00</start>
                <end>21-12-2011 12:00:00</end>
            </person>
        </people>""",
        'expected': [['i.ivanov', 'Start greater than end or some metric is on another date. Start is 2011-12-22 10:00:00', ' end is 2011-12-21 12:00:00']]
    },
    'test_end_on_another_day_than_start': {
        'input': """<?xml version="1.0" encoding="UTF-8"?>
        <people>
            <person full_name="i.ivanov">
                <start>21-12-2011 10:00:00</start>
                <end>22-12-2011 12:00:00</end>
            </person>
        </people>""",
        'expected': [['i.ivanov', 'Start greater than end or some metric is on another date. Start is 2011-12-21 10:00:00', ' end is 2011-12-22 12:00:00']]
    },
    'test_split_by_employee': {
        'input': """<?xml version="1.0" encoding="UTF-8"?>
        <people>
            <person full_name="i.ivanov">
                <start>21-12-2011 10:00:00</start>
                <end>21-12-2011 12:00:00</end>
            </person>
            <person full_name="s.petrov">
                <start>21-12-2011 11:00:00</start>
                <end>21-12-2011 18:00:00</end>
            </person>
        </people>""",
        'expected': [['21-12-2011', 'i.ivanov', '2:00:00'], ['21-12-2011', 's.petrov', '7:00:00']],
    },
    'test_employee_has_records_on_different_days': {
        'input': """<?xml version="1.0" encoding="UTF-8"?>
        <people>
            <person full_name="i.ivanov">
                <start>21-12-2011 10:00:00</start>
                <end>21-12-2011 12:00:00</end>
            </person>
            <person full_name="s.petrov">
                <start>21-12-2011 11:00:00</start>
                <end>21-12-2011 18:00:00</end>
            </person>
            <person full_name="i.ivanov">
                <start>22-12-2011 10:00:00</start>
                <end>22-12-2011 15:00:00</end>
            </person>
        </people>""",
        'expected': [['21-12-2011', 'i.ivanov', '2:00:00'], ['21-12-2011', 's.petrov', '7:00:00'], ['22-12-2011', 'i.ivanov', '5:00:00']],
    },
    'test_employee_has_few_records_on_single_days': {
        'input': """<?xml version="1.0" encoding="UTF-8"?>
        <people>
            <person full_name="i.ivanov">
                <start>21-12-2011 10:00:00</start>
                <end>21-12-2011 12:00:00</end>
            </person>
            <person full_name="s.petrov">
                <start>21-12-2011 11:00:00</start>
                <end>21-12-2011 18:00:00</end>
            </person>
            <person full_name="i.ivanov">
                <start>21-12-2011 13:00:00</start>
                <end>21-12-2011 19:00:00</end>
            </person>
        </people>""",
        'expected': [['21-12-2011', 'i.ivanov', '8:00:00'], ['21-12-2011', 's.petrov', '7:00:00']],
    },
    'test_file_contains_invalid_date_format': {
        'input': """<?xml version="1.0" encoding="UTF-8"?>
        <people>
            <person full_name="i.ivanov">
                <start>21-12-2011 10:00:00</start>
                <end>46-12-2011 12:00:00</end>
            </person>
        </people>""",
        'expected': [['Failed parse date string "46-12-2011 12:00:00"', " time data '46-12-2011 12:00:00' does not match format '%d-%m-%Y %H:%M:%S'"]],
    },
    'test_output_file_path_option_respected': {
        'input': """<?xml version="1.0" encoding="UTF-8"?>
        <people>
            <person full_name="i.ivanov">
                <start>21-12-2011 10:00:00</start>
                <end>21-12-2011 19:00:00</end>
            </person>
            <person full_name="s.petrov">
                <start>21-12-2011 11:00:00</start>
                <end>21-12-2011 18:00:00</end>
            </person>
        </people>""",
        'expected': [['21-12-2011', 'all', '16:00:00']],
    },
    'test_no_start_date_violator_recorded': {
        'input': """<?xml version="1.0" encoding="UTF-8"?>
        <people>
            <person full_name="i.ivanov">
                <end>21-12-2011 19:00:00</end>
            </person>
        </people>""",
        'expected': [['i.ivanov', 'Has no start date with end at 2011-12-21 19:00:00']],
    },
    'test_no_end_date_violator_recorded': {
        'input': """<?xml version="1.0" encoding="UTF-8"?>
        <people>
            <person full_name="i.ivanov">
                <start>21-12-2011 19:00:00</start>
            </person>
        </people>""",
        'expected': [['i.ivanov', 'Has no end date with start at 2011-12-21 19:00:00']],
    },
    'test_report_file_path_option_respected': {
        'input': """<?xml version="1.0" encoding="UTF-8"?>
            <people>
                <person full_name="i.ivanov">
                <start>21-12-2011 19:00:00</start>
            </person>
    </people>""",
        'expected': [['i.ivanov', 'Has no end date with start at 2011-12-21 19:00:00']],
    },
    'test_all_params_passed': {
        'input': """<?xml version="1.0" encoding="UTF-8"?>
        <people>
            <person full_name="i.ivanov">
                <start>21-12-2011 10:00:00</start>
                <end>21-12-2011 12:00:00</end>
            </person>
            <person full_name="s.petrov">
                <start>25-12-2011 11:00:00</start>
                <end>25-12-2011 18:00:00</end>
            </person>
            <person full_name="i.ivanov">
                <start>25-12-2011 12:00:00</start>
                <end>25-12-2011 22:00:00</end>
            </person>
            <person full_name="g.sidorov">
                <start>25-12-2011 12:00:00</start>
            </person>
            <person full_name="i.ivanov">
                <start>28-12-2011 13:00:00</start>
                <end>28-12-2011 19:00:00</end>
            </person>
        </people>""",
        'expected': (
            [['25-12-2011', 's.petrov', '7:00:00'], ['25-12-2011', 'i.ivanov', '10:00:00']],
            [['g.sidorov', 'Has no end date with start at 2011-12-25 12:00:00']]
        )
    },
}
