import argparse
import os
from zipfile import ZipFile
from lxml import etree
from datetime import datetime, timedelta
from collections import OrderedDict
from flask import Flask, request, render_template, send_file


XML_DATE_FORMAT = '%d-%m-%Y'
HTML_DATE_FORMAT = '%Y-%m-%d'
DEFAULT_GROUPING_MARKER = 'all'
DEFAULT_FILES_DIR = 'results'
DEFAULT_OUTPUT_FILENAME = 'worktime_summary.csv'
DEFAULT_REPORT_FILENAME = 'violators.csv'
ZIP_FILENAME = 'result.zip'
ZIP_PATH = os.path.join(DEFAULT_FILES_DIR, ZIP_FILENAME)
DEFAULT_OUTPUT_PATH = os.path.join(DEFAULT_FILES_DIR, DEFAULT_OUTPUT_FILENAME)
DEFAULT_REPORT_PATH = os.path.join(DEFAULT_FILES_DIR, DEFAULT_REPORT_FILENAME)


NO_CHECK_IN = 'no_check_in'
NO_CHECKOUT = 'no_checkout'
ABNORMAL_WORKTIME = 'abnormal_worktime'

VIOLATION_REASON_MAP = {
    NO_CHECK_IN: 'Has no start date with end at {}',
    NO_CHECKOUT: 'Has no end date with start at {}',
    ABNORMAL_WORKTIME: 'Start greater than end or some metric is on another date. Start is {}, end is {}',
}


app = Flask(__name__)


def parse_args() -> argparse.Namespace:
    """
    Returns object containing named cli args
    :return: cli args
    """
    parser = argparse.ArgumentParser()

    parser.add_argument('--source_path', help='Path to file with worktime xml')
    parser.add_argument('--output_path',
                        default=DEFAULT_OUTPUT_PATH,
                        help=f'Path to file with summarized results. Default is {DEFAULT_OUTPUT_PATH}')
    parser.add_argument('--report_path',
                        default=DEFAULT_REPORT_PATH,
                        help=f'Path to file with violators. Default is {DEFAULT_REPORT_PATH}')
    parser.add_argument('--by_employee', action='store_true', help='Split statistics per day by employee')
    parser.add_argument('--cli', action='store_true', help='Work as cli utility instead of web app')
    parser.add_argument('--date_start', help='Date to start calculate statistics from. Earliest if empty')
    parser.add_argument('--date_end', help='Date to end calculate statistics to. Latest if empty')

    args: argparse.Namespace = parser.parse_args()

    return args


def get_date_str_from_datetime(input_datetime: datetime) -> str:
    """
    Returns date part of datetime as str
    :param input_datetime: datetime obj
    :return: date str
    """
    return input_datetime.strftime(XML_DATE_FORMAT)


def get_time_value(elem: etree.Element) -> datetime:
    """
    Safely extracts date from xml as datetime objects. Report error in stdout if failed
    :param elem: xml element
    :return: datetime in the element
    """
    result_datetime = None
    if elem is not None:
        datetime_text = elem.text
        if datetime_text:
            try:
                result_datetime = datetime.strptime(datetime_text, '%d-%m-%Y %H:%M:%S')
            except ValueError as e:
                print(f'Failed parse date string "{datetime_text}", {str(e)}')
    return result_datetime


def get_dates_for_summarize(sum_from: str = None, sum_to: str = None) -> tuple:
    """
    Getting dates of period to summarize worktime as datetime from strs passed in cli args
    :param sum_from: count from (inclusive)
    :param sum_to: count to (inclusive)
    :return: datetime from, datetime to
    """
    sum_start_date = None
    if sum_from:
        # Work around to support date in cli args in the same format as dates in xml file and RFC format from html input
        try:
            sum_start_date = datetime.strptime(sum_from, HTML_DATE_FORMAT)
        except ValueError:
            sum_start_date = datetime.strptime(sum_from, XML_DATE_FORMAT)
    sum_end_date = None
    if sum_to:
        try:
            sum_end_date = datetime.strptime(sum_to, HTML_DATE_FORMAT)
        except ValueError:
            sum_end_date = datetime.strptime(sum_to, XML_DATE_FORMAT)
    return sum_start_date, sum_end_date


def get_timedelta_from_datetime(interval_datetime: datetime) -> timedelta:
    """
    Get timedelta from datetime to ease calculation of worktime during day
    :param interval_datetime: start or end of checked worktime interval
    :return: H:M:S as timedelta
    """
    return timedelta(hours=interval_datetime.hour, minutes=interval_datetime.minute, seconds=interval_datetime.second)


def is_sum_at_this_datetime(current_date: datetime, sum_from: datetime = None, sum_to: datetime = None) -> bool:
    """
    Check if given date should be summarized based on given cli args
    :param current_date: date to check
    :param sum_from: start period from cli args
    :param sum_to: end period of cli args
    :return: check result
    """
    result = True
    if sum_from and sum_to:
        result = sum_from <= current_date <= sum_to
    elif sum_from:
        result = current_date >= sum_from
    elif sum_to:
        result = current_date <= sum_to
    return result


def parse_source(file_path: str, sum_from: str = None, sum_to: str = None, by_employee: bool = False) -> tuple:
    """
    Summarize worktime based on given cli options
    :param file_path: xml source
    :param sum_from: interval start from cli args
    :param sum_to: interval end from cli args
    :param by_employee: split output by employee or summarize all workers per date
    :return: summary result and found violations
    """
    parser: etree.iterparse = etree.iterparse(file_path, events=('end', ), tag='person')

    parse_sum_from, parse_sum_to = get_dates_for_summarize(sum_from, sum_to)

    worktime_summary: dict = {}
    violators: dict = {
        NO_CHECK_IN: [],
        NO_CHECKOUT: [],
        ABNORMAL_WORKTIME: [],
    }

    for _, elem in parser:
        full_name: str = elem.attrib.get('full_name')
        start: datetime = get_time_value(elem.find('start'))
        end: datetime = get_time_value(elem.find('end'))

        if start is None:
            violators[NO_CHECK_IN].append((full_name, [end]))
        elif end is None:
            violators[NO_CHECKOUT].append((full_name, [start]))
        elif start > end:
            violators[ABNORMAL_WORKTIME].append((full_name, [start, end]))
        elif end.day - start.day >= 1:
            violators[ABNORMAL_WORKTIME].append((full_name, [start, end]))
        else:
            if is_sum_at_this_datetime(start, parse_sum_from, parse_sum_to):
                date: str = get_date_str_from_datetime(start)
                interval_starts: timedelta = get_timedelta_from_datetime(start)
                interval_ends: timedelta = get_timedelta_from_datetime(end)

                if by_employee:
                    summary_object: str = full_name
                else:
                    summary_object = DEFAULT_GROUPING_MARKER

                if date not in worktime_summary:
                    worktime_summary[date] = {}

                current_worktime: timedelta = worktime_summary.get(date, {}).get(summary_object, timedelta())
                worktime_summary[date][summary_object] = current_worktime + (interval_ends - interval_starts)

            # Required to manually cleanup references for processed elements to free up memory
            elem.clear()
            while elem.getprevious() is not None:
                del elem.getparent()[0]

    return worktime_summary, violators


def worktime_summary_to_csv(worktime_summary: dict, output_path: str) -> None:
    """
    Write summary to csv
    :param worktime_summary: result of summarizing
    :param output_path: file to write in
    :return:
    """
    if worktime_summary:
        with open(output_path, 'w', encoding='utf-8') as outfile:
            # 1. Will not rely of current dict implementation with guaranteed keys order
            # 2. Make sure unordered dates in source will not make mess in output
            summary_ordered_by_date = OrderedDict(sorted(worktime_summary.items()))
            for date, summary_object in summary_ordered_by_date.items():
                for object_name, worktime in summary_object.items():
                    outfile.write(f'{date},{object_name},{worktime}\n')


def generate_violators_to_report(violators: dict, report_path: str):
    """
    Write found violations
    :param violators: violations found while summarized worktime
    :param report_path: file to write in
    :return:
    """
    if violators:
        with open(report_path, 'w', encoding='utf-8') as report_file:
            for reason, violators_data in violators.items():
                report_file.write('\n'.join([f'{violator[0]},{VIOLATION_REASON_MAP[reason].format(*violator[1])}'
                                             for violator in violators_data]))


def get_zip_for_response() -> ZipFile:
    """
    Pack output and violations report for sending back to user
    :return: zip with reports
    """
    with ZipFile(ZIP_PATH, 'w') as zipfile:
        zipfile.write(DEFAULT_OUTPUT_PATH)
        zipfile.write(DEFAULT_REPORT_PATH)
        return zipfile


def clear_previous_results() -> None:
    """
    Clear previous results
    :return:
    """
    open(DEFAULT_OUTPUT_PATH, 'w').close()
    open(DEFAULT_REPORT_PATH, 'w').close()


def main(args: argparse.Namespace) -> None:
    """
    Entry point to run summarizing
    :return:
    """
    worktime_summary, violators = parse_source(args.source_path, args.date_start, args.date_end, args.by_employee)
    worktime_summary_to_csv(worktime_summary, args.output_path)
    generate_violators_to_report(violators, args.report_path)


@app.route('/upload', methods=['GET', 'POST'])
def handle_upload():
    if request.method == 'POST':
        source_file = request.files.get('source_file')
        if not source_file or source_file.content_type != 'text/xml':
            return render_template('error.html')
        by_employee = True if request.form.get('by_employee') == 'on' else False
        date_start = request.form.get('date_start')
        date_end = request.form.get('date_end')
        worktime_summary, violators = parse_source(source_file, date_start, date_end, by_employee)
        worktime_summary_to_csv(worktime_summary, DEFAULT_OUTPUT_PATH)
        generate_violators_to_report(violators, DEFAULT_REPORT_PATH)
        get_zip_for_response()
        return send_file(ZIP_PATH)
    else:
        return render_template('upload.html')


if __name__ == '__main__':
    clear_previous_results()
    args: argparse.Namespace = parse_args()
    if args.cli:
        try:
            main(args)
        except TypeError:
            print('--source_path option is required in cli mode')
    else:
        app.run(host='0.0.0.0')
